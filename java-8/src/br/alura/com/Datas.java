package br.alura.com;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Datas {

	public static void main(String[] args) {
		LocalDate hoje = LocalDate.now();
		System.out.println(hoje);
		LocalDate proximasOlimpiadas = LocalDate.of(2021, Month.JUNE, 5);
		proximasOlimpiadas = LocalDate.of(2025, Month.JUNE, 15);
		System.out.println(proximasOlimpiadas);
		Period periodo = Period.between(hoje, proximasOlimpiadas);
		System.out.println(
				periodo.getDays() + " dias, " + periodo.getMonths() + " meses e " + periodo.getYears() + " anos.");

		DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String valorFormatado = proximasOlimpiadas.format(formatador);
		System.out.println(valorFormatado);

		ZonedDateTime now = ZonedDateTime.now();
		System.out.println(now);

	}

}
