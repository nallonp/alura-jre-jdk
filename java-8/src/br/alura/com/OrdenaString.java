package br.alura.com;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class OrdenaString {

	public static void main(String[] args) {
		List<String> palavras = new ArrayList<>();
		palavras.add("alura online");
		palavras.add("casa do c�digo");
		palavras.add("caelum");

//		Comparator<String> comparador = new ComparadorDeStringPorTamanho();
//		Collections.sort(palavras, comparador);
//		System.out.println(palavras);

//		palavras.sort((s1, s2) -> Integer.compare(s1.length(), s2.length()));
//		Consumer<String> consumer = new ConsumidorDeString();
//		palavras.forEach(consumer);
		palavras.forEach(s -> System.out.println(s));

//		Comparator<String> comparator = Comparator.comparing(s -> s.length());
		palavras.sort(String.CASE_INSENSITIVE_ORDER);
//		palavras.forEach(s -> System.out.println(s));
		palavras.forEach(System.out::println);

	}

}

class ConsumidorDeString implements Consumer<String> {
	@Override
	public void accept(String s) {
		System.out.println(s);
	}
}

class ComparadorDeStringPorTamanho implements Comparator<String> {

	@Override
	public int compare(String s1, String s2) {
		return s1.length() - s2.length();
	}

}