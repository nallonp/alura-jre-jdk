package br.alura.com;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DatasExercicios {

	public static void main(String[] args) {
		LocalDate now = LocalDate.now();
		System.out.println(now);
		LocalDate future = LocalDate.of(2099, 1, 25);
		System.out.println(future);
		Period period = Period.between(now, future);
		System.out.println(period);
		DateTimeFormatter formater = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		System.out.println(now.format(formater));
		var a = "aaaa";
	}

}
