package br.alura.com;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class TestaCurso {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("Python", 45));
		cursos.add(new Curso("JavaScript", 150));
		cursos.add(new Curso("Java 8", 113));
		cursos.add(new Curso("C", 55));

		cursos.sort(Comparator.comparing(Curso::getAlunos));
//		cursos.forEach(c -> System.out.println(c.getNome()));

		int sum = cursos.stream()
				.filter(c -> c.getAlunos() > 100)
				.mapToInt(Curso::getAlunos)
				.sum();
//		System.out.println("Soma: " + sum);

		Optional<Curso> optionalCurso = cursos.stream()
				.filter(c -> c.getAlunos() >= 100)
				.findAny();

		Curso curso = optionalCurso.orElse(null);

//		System.out.println(curso.getNome());

		cursos = cursos.stream()
				.filter(c -> c.getAlunos() >= 100)
				.collect(Collectors.toList());

		cursos.forEach(c -> System.out.println(c.getNome()));

		Map<String, Integer> mapa = cursos.stream()
				.filter(c -> c.getAlunos() >= 100)
				.collect(Collectors.toMap(c -> c.getNome(), c -> c.getAlunos()));

		mapa.forEach((nome, aluno) -> System.out.println("Curso: " + nome + "\tAlunos: " + aluno));
	}

}
