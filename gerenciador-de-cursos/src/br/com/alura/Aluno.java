package br.com.alura;

public class Aluno {

	private String nome;
	private int numeroMatricula;

	public Aluno(String nome, int numemoMatricula) {
		if (nome == null) {
			throw new NullPointerException("Nome n�o pode ser nulo");
		}
		this.nome = nome;
		this.numeroMatricula = numemoMatricula;
	}

	public String getNome() {
		return nome;
	}

	public int getNumeroMatricula() {
		return numeroMatricula;
	}

	@Override
	public String toString() {
		return ("Aluno: " + nome + " - Matr�cula: " + numeroMatricula);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

//	@Override
//	public boolean equals(Object obj) {
//		if (!(obj instanceof Aluno)) {
//			throw new IllegalArgumentException("O objeto passado para compara��o n�o � do mesmo tipo da classe Aluno!");
//		}
//		Aluno aluno = (Aluno) obj;
//		return this.nome.equals(aluno.nome);
//	}
//
//	@Override
//	public int hashCode() {
//		return nome.hashCode();
//	}

}
