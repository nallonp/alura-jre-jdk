package br.com.alura;

import java.util.ArrayList;
import java.util.Collections;

public class TestandoListas {
	public static void main(String[] args) {
		String aula1 = "Conhecendo mais de listas";
		String aula2 = "Modelando a classe Aula";
		String aula3 = "Trabalhando com cursos e sets";
		String aula4 = "Aumentando nosso conhecimento";
		ArrayList<String> aulas = new ArrayList<>();
		aulas.add(aula1);
		aulas.add(aula2);
		aulas.add(aula3);
		aulas.add(aula4);
		// aulas.remove(0);
		// aulas.remove(aula2);
		// System.out.println(aulas);
		for (String aula : aulas) {
			System.out.println("Aula: " + aula);
		}
		String primeiraAula = aulas.get(0);
		System.out.println("A primeira aula �: " + primeiraAula);
		for (int i = 0; i < aulas.size(); i++) {
			System.out.println("Aula: " + aulas.get(i));
		}
		Collections.sort(aulas);
		aulas.forEach((aula) -> {
			System.out.print("Percorrendo: ");
			System.out.println(aula);
		});
	}
}
