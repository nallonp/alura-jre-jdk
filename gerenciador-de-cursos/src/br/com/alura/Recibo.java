package br.com.alura;

public class Recibo implements Comparable<Recibo> {
	private Double valor;
	// atributos da classe e m�todos acessadores/alteradores

	public Recibo(double valor) {
		this.valor = valor;
	}

	public double getValor() {
		return valor;
	}

	@Override
	public int compareTo(Recibo outro) {
		return valor.compareTo(outro.valor);
	}

	@Override
	public String toString() {
		return valor.toString();
	}

}