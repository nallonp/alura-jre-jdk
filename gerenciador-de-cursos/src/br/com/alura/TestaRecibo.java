package br.com.alura;

import java.util.TreeSet;

public class TestaRecibo {

	public static void main(String[] args) {

		TreeSet<Recibo> treeSet = new TreeSet<>();
		Recibo rec1 = new Recibo(100);
		Recibo rec2 = new Recibo(300);
		Recibo rec3 = new Recibo(200);

		treeSet.add(rec1);
		treeSet.add(rec2);
		treeSet.add(rec3);

		System.out.println(treeSet.size());
		treeSet.forEach(r -> System.out.println(r));

	}

}
