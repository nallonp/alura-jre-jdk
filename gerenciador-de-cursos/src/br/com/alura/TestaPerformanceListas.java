package br.com.alura;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

public class TestaPerformanceListas {

	public static void main(String[] args) {
		List<Long> list = new LinkedList<Long>();
		Instant start = Instant.now();
		for (long i = 0; i < 100000; i++) {
			list.add(0, i);
		}
		list.remove(0);
		Instant end = Instant.now();
		System.out.println(Duration.between(start, end));
	}

}
