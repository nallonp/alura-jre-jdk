package br.com.alura;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class Curso {
	private String nome;
	private String instrutor;
	private List<Aula> aulas = new LinkedList<Aula>();
	private Set<Aluno> alunos = new LinkedHashSet<Aluno>();
	private Map<Integer, Aluno> matriculaParaAluno = new HashMap<>();

	public Curso(String nome, String instrutor) {
		this.nome = nome;
		this.instrutor = instrutor;
	}

	public String getNome() {
		return nome;
	}

	public String getInstrutor() {
		return instrutor;
	}

	public List<Aula> getAulas() {
		return Collections.unmodifiableList(aulas);
	}

	public void adiciona(Aula outraAula) {
		this.aulas.add(outraAula);
	}

	public int tempoTotal() {
		int tempoTotal = 0;
		for (Aula aula : aulas) {
			tempoTotal += aula.getTempo();
		}
		return tempoTotal;
	}

	public void matricula(Aluno aluno) {
		this.alunos.add(aluno);
		this.matriculaParaAluno.put(aluno.getNumeroMatricula(), aluno);
	}

	public Set<Aluno> getAlunos() {
		return Collections.unmodifiableSet(alunos);
	}

	public boolean estaMatriculado(Aluno aluno) {
		return alunos.contains(aluno);
	}

	public Aluno buscaMatriculado(int matricula) {
		if (!matriculaParaAluno.containsKey(matricula)) {
			throw new NoSuchElementException("Matr�cula n�o encontrada (" + matricula + ").");
		}
		return matriculaParaAluno.get(matricula);
//		for (Aluno aluno : alunos) {
//			if (aluno.getNumeroMatricula() == matricula) {
//				return aluno;
//			}
//		}
//		throw new NoSuchElementException("Matr�cula n�o encontrada (" + matricula + ").");
	}

	@Override
	public String toString() {
		return String.format("Curso: %s - Tempo total: %s Aulas: %s", nome, tempoTotal(), aulas);
	}

}
