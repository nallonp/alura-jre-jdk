
public class Testereferencias {

	public static void main(String[] args) {
		Funcionario g1 = new Gerente();
		g1.setNome("Marcos");
		String nome = g1.getNome();
		Gerente g2 = (Gerente) g1;
		System.out.println(g2.autentica(1) + " - " + g2.getNome());
		System.out.println(nome);

	}

}
