package br.com.alura.java.io.teste;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class TesteEscritaPrintStream {
	public static void main(String[] args) throws IOException {
		try {
			PrintStream ps = new PrintStream("lorem2.txt", StandardCharsets.UTF_8);
			ps.println("Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
			ps.println();
			ps.println();
			ps.println("Nallon Pauluzzi de Barros ��");
			ps.close();
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo n�o encontrado!");
		}
	}

}
