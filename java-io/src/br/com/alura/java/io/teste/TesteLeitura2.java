package br.com.alura.java.io.teste;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Scanner;

public class TesteLeitura2 {

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(new File("contas.csv"), StandardCharsets.UTF_8);
		Scanner linhaScanner;
		while (scanner.hasNextLine()) {
			String linha = scanner.nextLine();
			// System.out.println(linha);
			linhaScanner = new Scanner(linha);
			linhaScanner.useDelimiter(",");
			linhaScanner.useLocale(Locale.US);
			String tipoConta = linhaScanner.next();
			int agencia = linhaScanner.nextInt();
			int numero = linhaScanner.nextInt();
			String titular = linhaScanner.next();
			double saldo = linhaScanner.nextDouble();

			String valorFomatado = String.format(new Locale("pt", "BR"), "%s \t %04d \t %08d \t %s \t %05.2f",
					tipoConta, agencia, numero, titular, saldo);
			System.out.println(valorFomatado);
			// String[] valores = linha.split(",");
//			System.out.println(Arrays.toString(valores));
		}
		scanner.close();
	}

}
