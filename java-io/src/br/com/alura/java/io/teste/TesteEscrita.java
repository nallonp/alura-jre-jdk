package br.com.alura.java.io.teste;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class TesteEscrita {
	public static void main(String[] args) {
		try {
			OutputStream fos = new FileOutputStream("lorem2.txt");
			Writer osr = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osr);
			bw.write("Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
			bw.newLine();
			bw.newLine();
			bw.write("Nallon Pauluzzi de Barros");
			bw.close();
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo n�o encontrado!");
		} catch (IOException e) {
			System.out.println("Erro ao ler o arquivo.");
		}
	}

}
