package br.com.alura.java.io.teste;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TesteUnicodeEEncoding {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String s = "�";
		String sNovo;
		byte[] bytes;
		System.out.println(s.codePointAt(0));
		System.out.println("Default charset: " + Charset.defaultCharset().displayName());

		bytes = s.getBytes("windows-1252");
		sNovo = new String(bytes, "windows-1252");
		System.out.println(bytes.length + "\t windows-1252 \t" + sNovo);

		bytes = s.getBytes("UTF-16");
		// Encoding incorreto na hora de criar a string traz problemas na leitura.
		sNovo = new String(bytes, "windows-1252");
		System.out.println(bytes.length + "\t UTF-16 \t" + sNovo);

		bytes = s.getBytes(StandardCharsets.US_ASCII);
		// Tabela n�o possui representa��o para �.
		sNovo = new String(bytes, StandardCharsets.US_ASCII);
		System.out.println(bytes.length + "\t US_ASCII \t" + sNovo);
	}

}
