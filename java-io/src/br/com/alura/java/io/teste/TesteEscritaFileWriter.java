package br.com.alura.java.io.teste;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class TesteEscritaFileWriter {
	public static void main(String[] args) throws InterruptedException {
		try {
			long ini = System.currentTimeMillis();
			// FileWriter fw = new FileWriter("lorem2.txt");
			BufferedWriter bw = new BufferedWriter(new FileWriter("lorem2.txt"));
			bw.write("Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
			bw.newLine();
			bw.newLine();
			// fw.write(System.lineSeparator());
			// fw.write(System.lineSeparator());
			bw.write("Nallon Pauluzzi de Barros");
			bw.close();
			long fim = System.currentTimeMillis();
			System.out.println("Passaram " + (fim - ini) + " milissegundos");
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo n�o encontrado!");
		} catch (IOException e) {
			System.out.println("Erro ao ler o arquivo.");
		}
	}

}
