package br.com.bytebank.banco.test;

import br.com.bytebank.banco.modelo.Estatico;

public class TesteString {

	public static void main(String[] args) {
		String name = "Alura";
		String test = name;
		name = name.replace("A", "a");
		System.out.println("name: " + name + " Test:" + test);

		String esp = "";
		System.out.println(esp.isBlank());

		char[] cname = name.toCharArray();
		for (char n : cname) {
			System.out.println(n);
		}

		String nome = "ALURA";
		CharSequence cs = new StringBuilder("al");

		nome = nome.replace("AL", cs);

		System.out.println(nome);

		Estatico.Testa();

	}

}
