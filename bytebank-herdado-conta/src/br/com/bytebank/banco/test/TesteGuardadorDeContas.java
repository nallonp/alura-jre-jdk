package br.com.bytebank.banco.test;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;
import br.com.bytebank.banco.modelo.GuardadorDeContas;

public class TesteGuardadorDeContas {

	public static void main(String[] args) {
		GuardadorDeContas guardador = new GuardadorDeContas();

		guardador.adiciona(new ContaCorrente(22, 11));
		guardador.adiciona(new ContaCorrente(22, 22));
		System.out.println(guardador.getQuantidadeDeElementos());
		Conta ref = guardador.getReferencia(0);
		System.out.println(ref);
	}

}
