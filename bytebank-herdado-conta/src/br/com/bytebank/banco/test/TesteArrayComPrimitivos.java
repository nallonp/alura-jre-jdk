package br.com.bytebank.banco.test;

public class TesteArrayComPrimitivos {
	public static void main(String[] args) {

		int[] idades = new int[99999];
//		idades[0] = 1;
//		idades[1] = 2;
//		idades[2] = 3;
//		idades[3] = 4;
//		idades[4] = 5;
		for (int i = 0; i < idades.length; i++) {
			idades[i] = i * 10;
		}
		for (int i : idades) {
			System.out.println(i);
		}
		System.out.println("Tamanho: " + idades.length);
	}
}
