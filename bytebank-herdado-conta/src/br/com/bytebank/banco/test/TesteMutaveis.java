package br.com.bytebank.banco.test;

import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteMutaveis {
	public static void main(String[] args) {
		// Imutáveis
		int a = 0;
		int b = a;
		a = 3;
		System.out.println("a: " + a + " b: " + b);

		ContaCorrente cc1 = new ContaCorrente(123, 123);
		ContaCorrente cc2 = cc1;
		cc1.setAgencia(456);
		cc1.setNumero(456);
		System.out.println(cc1);
		System.out.println(cc2);
	}
}
