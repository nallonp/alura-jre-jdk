package br.com.bytebank.banco.test;

import br.com.bytebank.banco.modelo.ContaCorrente;
import br.com.bytebank.banco.modelo.GuardadorDeObjetos;

public class TestGuardadorDeObjetos {
	public static void main(String[] args) {
		GuardadorDeObjetos guardador = new GuardadorDeObjetos();

		guardador.adiciona(new String("Ol� mundo!"));
		guardador.adiciona(new ContaCorrente(123, 312));

		System.out.println(guardador.getQuantidadeDeElementos());
	}
}
