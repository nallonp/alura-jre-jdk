package br.com.bytebank.banco.test.util;

import java.util.ArrayList;
import java.util.List;

public class TesteWrapperInteger {

	public static void main(String[] args) {

		int[] idades = new int[5];
		String[] nomes = new String[5];

		int idade = 29; // Integer
		Integer idadeRef = Integer.valueOf(29);
		int primitivo = idadeRef.intValue();

		List<Integer> numeros = new ArrayList<Integer>();
		numeros.add(29); // Autoboxing

		Number refNumero = Float.valueOf(29.9f);

		List<Number> lista = new ArrayList<>();
		lista.add(10);
		lista.add(32.6);
		lista.add(25.6f);

		System.out.println(Integer.MAX_VALUE); // 2^31 - 1
		System.out.println(Integer.MIN_VALUE); // -2^31

		System.out.println(Integer.SIZE); // 32 bits
		System.out.println(Integer.BYTES); // 4 Bytes
	}

}
