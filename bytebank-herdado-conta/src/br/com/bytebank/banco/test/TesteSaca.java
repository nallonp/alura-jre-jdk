package br.com.bytebank.banco.test;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteSaca {
	public static void main(String[] args) {
		Conta cc = new ContaCorrente(13, 45);
		cc.deposita(200);
		try {
			cc.saca(400);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}
}
