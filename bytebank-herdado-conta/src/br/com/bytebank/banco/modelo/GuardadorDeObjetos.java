package br.com.bytebank.banco.modelo;

public class GuardadorDeObjetos {
	private Object[] referencias;
	private int posicaoLivre;

	public GuardadorDeObjetos() {
		this.referencias = new Object[10];
		this.posicaoLivre = 0;
	}

	public void adiciona(Object obj) {
		this.referencias[this.posicaoLivre] = obj;
		posicaoLivre++;
	}

	public int getPosicaoLivre() {
		return posicaoLivre;
	}

	public int getQuantidadeDeElementos() {
		return this.posicaoLivre;
	}

	public Object getReferencia(int position) {
		return referencias[position];
	}
}
