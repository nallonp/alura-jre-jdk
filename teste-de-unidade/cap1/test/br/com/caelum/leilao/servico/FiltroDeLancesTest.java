package br.com.caelum.leilao.servico;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Usuario;

public class FiltroDeLancesTest {

	@Test
	public void deveSelecionarLancesEntre1000E3000() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(Arrays.asList(new Lance(joao, 2000), new Lance(joao, 1000),
				new Lance(joao, 3000), new Lance(joao, 800)));

		assertEquals(1, resultado.size());
		assertEquals(2000, resultado.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void deveSelecionarLancesEntre500E700() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(
				Arrays.asList(new Lance(joao, 600), new Lance(joao, 500), new Lance(joao, 700), new Lance(joao, 800)));

		assertEquals(1, resultado.size());
		assertEquals(600, resultado.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void deveSelecionarLancesMaioresQue5000() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(Arrays.asList(new Lance(joao, 800), new Lance(joao, 5001)));

		assertEquals(1, resultado.size());
		assertEquals(5001, resultado.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void deveEliminarLancesMenoresQue500() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(
				Arrays.asList(new Lance(joao, 300), new Lance(joao, 301), new Lance(joao, 150), new Lance(joao, 501)));

		assertEquals(1, resultado.size());
		assertEquals(501, resultado.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void deveEliminarLancesEntre700e1000() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(Arrays.asList(new Lance(joao, 700), new Lance(joao, 1000),
				new Lance(joao, 850), new Lance(joao, 1001)));

		assertEquals(1, resultado.size());
		assertEquals(1001, resultado.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void deveEliminarLancesEntre3000e5000() {
		Usuario joao = new Usuario("Joao");

		FiltroDeLances filtro = new FiltroDeLances();
		List<Lance> resultado = filtro.filtra(Arrays.asList(new Lance(joao, 3000), new Lance(joao, 4000),
				new Lance(joao, 5000), new Lance(joao, 5001)));

		assertEquals(1, resultado.size());
		assertEquals(5001, resultado.get(0)
				.getValor(), 0.00001);
	}

}