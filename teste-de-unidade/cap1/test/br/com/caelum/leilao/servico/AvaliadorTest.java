package br.com.caelum.leilao.servico;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Usuario;

public class AvaliadorTest {
	private Avaliador leiloeiro;
	private Usuario joao;
	private Usuario jose;
	private Usuario maria;

	@Before
	public void setUp() {
		this.leiloeiro = new Avaliador();
		joao = new Usuario("Jo�o");
		jose = new Usuario("Jos�");
		maria = new Usuario("Maria");
	}

	@Test(expected = RuntimeException.class)
	public void naoDeveAvaliarLeiloesSemNenhumLanceDado() {
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.constroi();
		leiloeiro.avalia(leilao);

	}

	@Test
	public void deveEntenderLancesEmOrdemCrescente() {
		// Cen�rio:
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 250.0)
				.lance(jose, 300.0)
				.lance(maria, 400.0)
				.constroi();
		// A��o:
		leiloeiro.avalia(leilao);

		// Valida��o:
		assertThat(leiloeiro.getMaiorLance(), equalTo(400.0));
		assertThat(leiloeiro.getMenorLance(), equalTo(250.0));
	}

	@Test
	public void deveEntenderLancesEmOrdemDecrescente() {
		// Cen�rio:
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 400.0)
				.lance(maria, 300.0)
				.lance(joao, 200.0)
				.lance(maria, 100.0)
				.constroi();
		// A��o:
		leiloeiro.avalia(leilao);

		// Valida��o:
		assertThat(leiloeiro.getMaiorLance(), equalTo(400.0));
		assertThat(leiloeiro.getMenorLance(), equalTo(100.0));
	}

	@Test
	public void deveEntenderLancesEmOrdemAleatoria() {
		// Cen�rio:
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(maria, 200.0)
				.lance(jose, 450.0)
				.lance(joao, 120)
				.lance(jose, 700)
				.lance(joao, 630.0)
				.lance(maria, 230.0)
				.constroi();

		// A��o:
		leiloeiro.avalia(leilao);

		// Valida��o:
		assertThat(leiloeiro.getMaiorLance(), equalTo(700.0));
		assertThat(leiloeiro.getMenorLance(), equalTo(120.0));
	}

	@Test
	public void deveCalcularAMedia() {

		// Cen�rio:
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 200.0)
				.lance(jose, 300.0)
				.lance(maria, 400.0)
				.constroi();

		// A��o:
		leiloeiro.avalia(leilao);

		assertThat(leiloeiro.getMedia(leilao), equalTo(300.0));
	}

	@Test
	public void deveEntenderLeilaoComApenasUmLance() {
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 1000.0)
				.constroi();

		leiloeiro.avalia(leilao);

		assertThat(leiloeiro.getMaiorLance(), equalTo(1000.0));
		assertThat(leiloeiro.getMenorLance(), equalTo(1000.0));
	}

	@Test
	public void deveEncontrarOsTresMaiores() {
		Leilao leilao = new CriadorDeLeilao().para("Playstation 3 Novo")
				.lance(joao, 100.0)
				.lance(maria, 200.0)
				.lance(joao, 300.0)
				.lance(maria, 400.0)
				.constroi();

		leiloeiro.avalia(leilao);

		List<Lance> maiores = leiloeiro.getTresMaiores();
		assertThat(maiores.size(), equalTo(3));
		assertThat(maiores, hasItems(new Lance(maria, 400), new Lance(joao, 300), new Lance(maria, 400)));
	}

}
