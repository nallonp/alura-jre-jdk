package br.com.caelum.leilao.dominio;

import static br.com.caelum.leilao.matcher.LeilaoMatcher.temUmLance;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.leilao.builder.CriadorDeLeilao;

public class LeilaoTest {

	private Usuario steve;
	private Usuario bill;
	private Usuario wos;

	@Before
	public void setUp() {
		steve = new Usuario("Steve Jobs");
		bill = new Usuario("Bill Gates");
		wos = new Usuario("Steve Wozniak");
	}

	@Test
	public void deveReceberUmLance() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.constroi();

		assertEquals(0, leilao.getLances()
				.size());
		Lance lance = new Lance(new Usuario("Steve Jobs"), 2000);
		leilao.propoe(lance);
		assertThat(leilao.getLances()
				.size(), equalTo(1));
		assertThat(leilao, temUmLance(lance));
	}

	@Test
	public void deveReceberVariosLances() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.constroi();
		assertEquals(0, leilao.getLances()
				.size());

		leilao.propoe(new Lance(steve, 2000));
		leilao.propoe(new Lance(wos, 3000));
		assertEquals(2, leilao.getLances()
				.size());
		assertEquals(2000, leilao.getLances()
				.get(0)
				.getValor(), 0.00001);
		assertEquals(3000, leilao.getLances()
				.get(1)
				.getValor(), 0.00001);
	}

	@Test
	public void naoDeveAceitarDoisLancesSeguidosDoMesmoUsuario() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.lance(steve, 2000)
				.lance(steve, 3000)
				.constroi();

		assertEquals(1, leilao.getLances()
				.size());
		assertEquals(2000.0, leilao.getLances()
				.get(0)
				.getValor(), 0.00001);
	}

	@Test
	public void naoDeveAceitarMaisQueCincoLancesDoMesmoUsuario() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.lance(steve, 2000)
				.lance(bill, 3000)
				.lance(steve, 4000)
				.lance(bill, 5000)
				.lance(steve, 6000)
				.lance(bill, 7000)
				.lance(steve, 8000)
				.lance(bill, 9000)
				.lance(steve, 10000)
				.lance(bill, 11000)
				.constroi();

		// deve ser ignorado
		leilao.propoe(new Lance(steve, 12000));

		assertEquals(10, leilao.getLances()
				.size());

		assertEquals(11000, leilao.getLances()
				.get(leilao.getLances()
						.size() - 1)
				.getValor(), 0.00001);

	}

	@Test
	public void deveDobrarOLance() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.constroi();
		leilao.dobraLance(steve);
		leilao.propoe(new Lance(steve, 1500));
		leilao.propoe(new Lance(bill, 2000));

		leilao.dobraLance(steve);

		assertEquals(3, leilao.getLances()
				.size());

		assertEquals(1500.0, leilao.getLances()
				.get(0)
				.getValor(), 0.00001);

		assertEquals(2000.0, leilao.getLances()
				.get(1)
				.getValor(), 0.00001);

		assertEquals(3000.0, leilao.getLances()
				.get(2)
				.getValor(), 0.00001);

	}

	@Test
	public void naoDeveDobrarCasoNaoHajaLanceAnterior() {
		Leilao leilao = new CriadorDeLeilao().para("Macbook Pro 15\"")
				.constroi();
		leilao.dobraLance(steve);
		assertEquals(0, leilao.getLances()
				.size());
	}

}
