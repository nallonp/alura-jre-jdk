package br.com.caelum.leilao.dominio;

import org.junit.Before;
import org.junit.Test;

public class LanceTest {
	private Usuario steve;

	@Before
	public void setUp() {
		steve = new Usuario("Steve Jobs");
	}

	@Test(expected = IllegalArgumentException.class)
	public void naoDeveReceberLanceComValor0OuNegativo() {
		new Lance(steve, 0);
	}
}
