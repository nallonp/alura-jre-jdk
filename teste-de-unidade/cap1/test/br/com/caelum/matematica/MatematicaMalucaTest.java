package br.com.caelum.matematica;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class MatematicaMalucaTest {
	@Test
	public void deveEntenderValorMaiorQueTrinta() {
		MatematicaMaluca matematicaMaluca = new MatematicaMaluca();
		int num = 31;
		int contaMaluca = matematicaMaluca.contaMaluca(num);
		assertEquals(num * 4, contaMaluca);
	}

	@Test
	public void deveEntenderValorIgualATrinta() {
		MatematicaMaluca matematicaMaluca = new MatematicaMaluca();
		int num = 30;
		int contaMaluca = matematicaMaluca.contaMaluca(num);
		assertEquals(num * 3, contaMaluca);
	}

	@Test
	public void deveEntenderValorMaiorQueDezEMenorQueTrinta() {
		MatematicaMaluca matematicaMaluca = new MatematicaMaluca();
		int num = 25;
		int contaMaluca = matematicaMaluca.contaMaluca(num);
		assertEquals(num * 3, contaMaluca);
	}

	@Test
	public void deveEntenderValorIgualADez() {
		MatematicaMaluca matematicaMaluca = new MatematicaMaluca();
		int num = 10;
		int contaMaluca = matematicaMaluca.contaMaluca(num);
		assertEquals(num * 2, contaMaluca);
	}

	@Test
	public void deveEntenderValorMenorQueDez() {
		MatematicaMaluca matematicaMaluca = new MatematicaMaluca();
		int num = -1;
		int contaMaluca = matematicaMaluca.contaMaluca(num);
		assertEquals(num * 2, contaMaluca);
	}
}
